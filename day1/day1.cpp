#include "day1.hpp"


void doDay1() {

    std::array<std::string, 10> NUMBERS = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
                                           "nine"};
    std::vector<std::string> data = readData("../day1/input.txt");

    long total = 0;
    int count = 0;
    for (const auto &line: data) {
//        std::cout << "Line: " << line << std::endl;
        std::vector<char> chars(line.begin(), line.end());
        std::vector<int> numericChars;
        for (int i = 0; i < chars.size(); i++) {
            const char &c = chars[i];
//            std::cout << "Char: " << c << "\n";
            if (std::isdigit(c)) {
                numericChars.push_back(std::stoi(std::string(1, c)));
//                std::cout << "Is digit" << std::endl;
                continue;
            }
            for (int n=0; n<NUMBERS.size(); n++) {
//                std::cout << "\tChecking number: " << NUMBERS[n] << "\n";
                if (NUMBERS.at(n).length() > line.size()-i) {
//                    std::cout << "\t\tNot enough chars left\n";
                    continue; // skip if not enough chars left
                }
                bool found = true;
                for  (int j=0; j<NUMBERS[n].size(); j++) {
                    if (NUMBERS[n][j] != chars.at(i+j)) {
//                        std::cout << "\t\t " << NUMBERS[n][j] << " != " << chars.at(i+j) << std::endl;
                        found = false;
                        break;
                    } else {
//                        std::cout << "\t\t " << NUMBERS[n][j] << " == " << chars.at(i+j) << std::endl;
                    }
                }
                if (found) {
//                    std::cout << "Found number: " << NUMBERS[n] << " " << n << std::endl;
                    numericChars.push_back(n);
                }
            }
        }

        std::string numericString;
        numericString += std::to_string(numericChars[0]);
        numericString += std::to_string(numericChars[numericChars.size() - 1]);
        int result = std::stoi(numericString);
        total += result;
        count++;
    }

    std::cout << "Total: " << total <<
              std::endl;
}
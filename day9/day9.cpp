#include "day9.hpp"

std::vector<int> split(std::string s, std::string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::vector<int> res;

    while ((pos_end = s.find(delimiter, pos_start)) != std::string::npos) {
        token = s.substr(pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back(std::stoi(token));
    }

    res.push_back(std::stoi(s.substr(pos_start)));
    return res;
}

std::vector<int> calcDiffVec(std::vector<int> vec) {
    std::vector<int> res;
    for (int i = 0; i < vec.size() - 1; i++) {
        int current = vec[i];
        int next = vec[i + 1];
        int diff = next - current;
        res.push_back(diff);
    }
    return res;
}

void doDay9() {
    std::cout << "Start day 9!" << std::endl;

    std::vector<std::string> data = readData("../day9/input.txt");

    long total = 0;
    long totalFirst = 0;
    int count = 0;
    for (const auto &line: data) {
//        std::cout << "Line: " << line << std::endl;

        std::vector<int> tokens = split(line, " ");
        std::vector<std::vector<int>> allDiffs;
        allDiffs.push_back(tokens);

        bool allZeros = true;
        do {
            allZeros = true;
            auto diffs = calcDiffVec(allDiffs.at(allDiffs.size() - 1));
            allDiffs.push_back(diffs);
//            std::cout << "Num Vectors: " << allDiffs.size() << std::endl;
//            for (const auto &diff: diffs) {
//                std::cout << diff << " ";
//            }
//            std::cout << std::endl;
            for (const int &diff: diffs) {
                if (diff != 0) {
//                    std::cout << diff << " != 0" << std::endl;
                    allZeros = false;
                    break;
                }
            }
        } while (!allZeros);

        // Jetzt haben wir alle Differenzen
//        std::cout << "Alles gefunden\n" << std::endl;
        int firstValue = 0;
        int lastValue = 0;
        for (int i = allDiffs.size() - 1; i >= 0; i--) {
//            for (auto diff: allDiffs.at(i)) {
//                std::cout << diff << " ";
//            }
//            std::cout << std::endl;
            auto &diffs = allDiffs.at(i);
//            diffs.push_back(diffs.at(diffs.size() - 1) + lastValue);
            diffs.insert(diffs.begin(), diffs.at(0) - firstValue);
            lastValue = diffs.at(diffs.size() - 1);
            firstValue = diffs.at(0);

        }
//        std::cout << "Last value: " << lastValue << " first value: " << firstValue << "\n" << std::endl;
        total += lastValue;
        totalFirst += firstValue;
//        for (const auto& diff : allDiffs) {
//            for (const auto& d : diff) {
//                std::cout << d << " ";
//            }
//            std::cout << std::endl;
//        }
//        std::cout << std::endl;
        count++;
//        if (count > 1) {
//            break;
//        }
    }
    std::cout << "Total: " << total << std::endl;
    std::cout << "Total first: " << totalFirst << std::endl;

    std::cout << "End day 9!" << std::endl;
}
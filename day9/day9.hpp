//
// Created by joero on 09.12.2023.
//

#ifndef ADVENT2023_DAY9_HPP
#define ADVENT2023_DAY9_HPP

#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include "../utils.hpp"

void doDay9();

#endif //ADVENT2023_DAY9_HPP

#include <iostream>

#include "day1/day1.hpp"
#include "day2/day2.hpp"
#include "day9/day9.hpp"

int main() {
    std::cout << "Hello, World!" << std::endl;

    doDay9();

    return 0;
}
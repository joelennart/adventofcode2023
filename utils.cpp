#include "utils.hpp"

std::vector<std::string> readData(std::string filename) {
    std::vector<std::string> data;

    std::ifstream input(filename);

    if (input.is_open()) {
        std::string line;
        while (std::getline(input, line)) {
            data.push_back(line);
        }
        input.close();
    }

    std::cout << "Data size: " << data.size() << std::endl;
    return data;
}
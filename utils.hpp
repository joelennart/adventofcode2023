
#ifndef ADVENT2023_UTILS_HPP
#define ADVENT2023_UTILS_HPP

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

std::vector<std::string> readData(std::string filename);

#endif //ADVENT2023_UTILS_HPP
